#ifndef __RIKERIO_MMU_H__
#define __RIKERIO_MMU_H__

#include "string"
#include "filesystem"
#include "semaphore.h"

namespace rikerio {

class MMU {

public:

    static void create(const std::filesystem::path&, const size_t size);
    static void remove(const std::filesystem::path&, bool purge = false);

private:

    struct runtime_st {

        std::string id;
        std::string group_name;

        gid_t gid;

        mode_t dir_mode;
        mode_t file_mode;

        struct {
            struct {
                int protection;
                int visibility;
                size_t size;
            } shm;
        } profile;

    };


    static void set_effective_gid(struct runtime_st& runtime);
    static void init_runtime_data(struct runtime_st&);
    static void parse_config(struct runtime_st&, const std::string);
    static void apply_group_and_rights(struct runtime_st&, const std::string path, mode_t mode);
    static void apply_group_and_rights(struct runtime_st&, int fd, mode_t mode);
    static void check_and_create_folder(struct runtime_st&, const std::string);
    static void check_and_create_link(struct runtime_st&, const std::string link, const std::string target);
    static int check_and_create_file(struct runtime_st&, const std::string, int);

};

}


#endif
