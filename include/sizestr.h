#ifndef __RIKERIO_MMU_SIZESTR_H__
#define __RIKERIO_MMU_SIZESTR_H__

#include "regex"
#include "string"
#include "tgmath.h"

class Size {

private:

public:


    Size(const std::string& str) : size(0) {

        static std::regex unit_regex("^(\\d*(?:.\\d+)?)(B|K|M)?$");

        float fsize = 0.0f;
        float factor = 1.0f;

        std::smatch match;
        if (!std::regex_match(str, match, unit_regex)) {
            return;
        }

        if (match.size() == 3) {
            fsize = std::stof(match[1]);

            if (match[2] == "B") {
                factor = 1.0f;
            } else if (match[2] == "K") {
                factor = 1000.0f;
            } else if (match[2] == "M") {
                factor = pow(1000.0f, 2);
            }

        } else {
            return;
        }

        float ffsize = fsize * factor;

        if (ffsize <= 1.0f) {
            return;
        }

        error = false;
        size = ffsize;

    }

    operator size_t () {
        return size;
    }

    bool is_error() const {
        return error;
    }

private:

    size_t size;
    bool error = true;

};

#endif
