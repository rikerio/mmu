#include "gmock/gmock.h"
#include "sizestr.h"

TEST(RegEx, Positives) {

    Size s1("4096");

    ASSERT_EQ(size_t(4096), size_t(s1));

    Size s2("4096B");

    ASSERT_EQ(size_t(4096), size_t(s2));

    Size s3("4K");

    ASSERT_EQ(size_t(4000), size_t(s3));

    Size s4("4M");

    ASSERT_EQ(size_t(4 * (1000 * 1000)), size_t(s4));

    Size s5("0.004M");

    ASSERT_EQ(size_t(4000), size_t(s5));

    Size s6("0.004K");

    ASSERT_EQ(size_t(4), size_t(s6));

    Size s7("0.004B");

    ASSERT_EQ(size_t(0), size_t(s7));
    ASSERT_TRUE(s7.is_error());

    Size s8("abc");

    ASSERT_EQ(size_t(0), size_t(s8));
    ASSERT_TRUE(s8.is_error());

}


