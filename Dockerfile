FROM rikerio-automation:latest

RUN apt update
RUN apt install -y libyaml-cpp-dev 

RUN wget https://gitlab.com/rikerio/debian/rikerio-mmu/-/jobs/2813711095/artifacts/raw/build/rikerio-mmu_3.0.0_amd64.deb

RUN apt install -y ./rikerio-mmu_3.0.0_amd64.deb

CMD /usr/bin/rio mmu create --profile=default --config=/etc/rikerio/default.conf
