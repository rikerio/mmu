#/usr/bin/env bash

_rioSubCommands()
{

    local cur=${COMP_WORDS[COMP_CWORD]}
    local profile=io

    if [ -z ${RIO_PROFILE} ]; 
    then
      profile=default
    else
      profile=${RIO_PROFILE}
    fi

    local dataIdx=0
    local linkIdx=0
    local lsIdx=0
    local catIdx=0
    local inspectIdx=0
    local rmIdx=0
    local addIdx=0
    local dataFileIdx=0
    local linkFileIdx=0
    local idx=1


    for (( i=0; i < ${#COMP_WORDS[@]}; i++)); 
    do
      local o=${COMP_WORDS[$i]}
      case $o in
          "rio") ;;
          "data") dataIdx=$idx; ((idx++)) ;;
          "link") linkIdx=$idx; ((idx++)) ;;
          "ls") lsIdx=$idx; ((idx++)) ;;
          "cat") catIdx=$idx; ((idx++)) ;;
          "inspect") inspectIdx=$idx; ((idx++)) ;;
          "rm") rmIdx=$idx; ((idx++)) ;;
          "add") addIdx=$idx; ((idx++)) ;;
          -i=*|--id=*) profile="${o#*=}" ;;
          "") ;;
          *)

	      if [ $linkIdx -ne "0" ];
	      then
	        if [ $catIdx -ne "0" ] || [ $inspectIdx -ne "0" ] || [ $rmIdx -ne "0" ] || [ $addIdx -ne "0" ];
	        then
		  if [ $linkFileIdx -ne "0" ] && [ $dataFileIdx -eq "0" ];
		  then
	 	    dataFileIdx=$idx
		    ((idx++))
		  fi
	          if [ $linkFileIdx -eq "0" ];
	          then
		    linkFileIdx=$idx
		    ((idx++))
		  fi

	        fi
	      fi

	      if [ $dataIdx -ne "0" ] && [ $catIdx -ne "0" ];
	      then
	        if [ $dataFileIdx -eq "0" ];
	        then
		  dataFileIdx=$idx
		  ((idx++))
		fi
	      fi
	      
         ;;
       esac
    done

    local data=""
    data+=$dataIdx
    data+=$lsIdx
    data+=$catIdx
    data+=$dataFileIdx

    local link=""
    link+=$linkIdx
    link+=$lsIdx
    link+=$catIdx
    link+=$inspectIdx
    link+=$rmIdx
    link+=$addIdx
    link+=$linkFileIdx
    link+=$dataFileIdx

    COMPREPLY=()

    # rio ?
    if [ $data -eq "0000" ] && [ $link -eq "00000000" ];
    then
      local IFS=$' \t\n'
      local opts="data link version"
      COMPREPLY=($(compgen -W "$opts" -- ${cur}))
    fi

    # rio data ?
    if [ $data -eq "1000" ];
    then
      local IFS=$' \t\n'
      COMPREPLY=($(compgen -W "ls cat" -- ${cur}))
    fi

    # rio data cat ?
    if [ $data -eq "1020" ] || [ $data -eq "1023" ];
    then
      datafiles=$(ls -N /var/run/rikerio/${profile}/data/ 2> /dev/null)
      local IFS=$'\n'
      files=()
      files+="$datafiles"

      COMPREPLY+=($(compgen -W "$files" -- ${cur}))
    fi

    # rio link ?
    if [ $link -eq "10000000" ];
    then
      local IFS=$' \t\n'
      COMPREPLY=($(compgen -W "ls cat inspect rm add" -- ${cur}))
    fi


    # rio link inspect
    if [ $link -eq "10020000" ] || [ $link -eq "10020030" ];
    then
      links=$(ls -N /var/run/rikerio/${profile}/links/ 2> /dev/null)
      local IFS=$'\n'
      files=()
      files+="$links"
      COMPREPLY+=($(compgen -W "$files" -- ${cur}))
    fi


    # rio link cat ? || rio link inspect ? || rio link rm ? || rio link add ?
    if [ $link -eq "10200000" ] || [ $link -eq "10020000" ] || [ $link -eq "10002000" ] || [ $link -eq "10000200" ] || [ $link -eq "10200030" ] || [ $link -eq "10020030" ] || [ $link -eq "10002030" ] || [ $link -eq "10000230" ]; 
    then
      as=$(ls -N /var/run/rikerio/${profile}/links/ 2> /dev/null)
      local IFS=$'\n'
      files=()
      files+="$as"
      COMPREPLY+=($(compgen -W "$files" -- ${cur}))
    fi

    # rio links rm linkFile ? || rio link add linkFile ?
    if [ $link -eq "10002030" ] || [ $link -eq "10002034" ] || [ $link -eq "10000230" ] || [ $link -eq "10000234" ];
    then
      links=$(ls -N /var/run/rikerio/${profile}/data/ 2> /dev/null)
      local IFS=$'\n'
      files=()
      files+="$links"
      COMPREPLY+=($(compgen -W "$files" -- ${cur}))
    fi

}

COMP_WORDBREAKS=$'\"\'><;|&(: '

complete -o nospace -F _rioSubCommands rio

