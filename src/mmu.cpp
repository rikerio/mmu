#include "mmu.h"

#include "filesystem"
#include "iostream"
#include "fcntl.h"
#include "string.h"
#include "dirent.h"
#include "unistd.h"
#include "grp.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/mman.h"
#include "rikerio/semver.h"
#include "rikerio-automation/rikerio/rikerio.h"
#include "version.h"
#include "yaml-cpp/yaml.h"

namespace fs = std::filesystem;

void rikerio::MMU::set_effective_gid(struct runtime_st& runtime) {

    /* get group */
    struct group* grp = getgrnam(runtime.group_name.c_str());

    if (!grp) {
        throw std::runtime_error("no group " + runtime.group_name + "found!");
    }

    if (setegid(grp->gr_gid) == -1) {
        throw std::runtime_error("Error setting effective group id (" + std::string(strerror(errno)) + ")");
    }

}

void rikerio::MMU::init_runtime_data (struct runtime_st& runtime) {

    runtime.dir_mode = 0770;
    runtime.file_mode = 0664;

    runtime.group_name = "rikerio";

    runtime.profile.shm.size = 4096;
    runtime.profile.shm.protection = PROT_READ | PROT_WRITE;
    runtime.profile.shm.visibility = MAP_SHARED;

}

void rikerio::MMU::parse_config(struct runtime_st& runtime, const std::string filename) {

    YAML::Node node = YAML::LoadFile(filename);

    if (!node || node.Type() != YAML::NodeType::Map) {
        throw std::runtime_error("Config content musst be a YAML Map");
    }

    auto nSize = node["size"];

    if (nSize && nSize.Type() == YAML::NodeType::Scalar) {
        runtime.profile.shm.size = nSize.as<size_t>();
    }

}

void rikerio::MMU::remove(const std::filesystem::path& profile_path, bool purge) {

    runtime_st runtime;

    init_runtime_data(runtime);

    set_effective_gid(runtime);

    /* remove rikerios own memory area */

    try {
        RikerIO::Profile profile;
        RikerIO::init(profile_path, profile);

        // if the init was successfull deallocation initial mmu
        RikerIO::Allocation rio_alloc;
        RikerIO::DataPoint dp_version_major(RikerIO::Type::UINT16, 2);
        RikerIO::DataPoint dp_version_minor(RikerIO::Type::UINT16, 2);
        RikerIO::DataPoint dp_version_patch(RikerIO::Type::UINT16, 2);

        RikerIO::Data::remove(profile, "rikerio.version.major");
        RikerIO::Data::remove(profile, "rikerio.version.minor");
        RikerIO::Data::remove(profile, "rikerio.version.patch");

        RikerIO::dealloc(profile, "rikerio-mmu");

    } catch (std::exception& e) {
    }


    RikerIO::remove(profile_path, purge);

}

void rikerio::MMU::create(const std::filesystem::path& profile_path, const size_t size) {

    SemanticVersion version(RIKERIO_MMU_VERSION);

    runtime_st runtime;

    init_runtime_data(runtime);

    runtime.profile.shm.size = size;

    set_effective_gid(runtime);
    umask(0117);

    /* get pid */

    RikerIO::create(profile_path, size);

    /* setup rikerios own memory area */

    uint16_t version_major = version.get_major();
    uint16_t version_minor = version.get_minor();
    uint16_t version_patch = version.get_patch();

    RikerIO::Allocation rio_alloc;
    RikerIO::DataPoint dp_version_major(RikerIO::Type::UINT16, 2);
    RikerIO::DataPoint dp_version_minor(RikerIO::Type::UINT16, 2);
    RikerIO::DataPoint dp_version_patch(RikerIO::Type::UINT16, 2);

    RikerIO::Profile profile = { };
    RikerIO::init(profile_path, profile);
    RikerIO::alloc(profile, 6, "rikerio-mmu", rio_alloc);

    RikerIO::Data::set(profile, rio_alloc, "rikerio.version.major", dp_version_major);
    RikerIO::Data::set(profile, rio_alloc, "rikerio.version.minor", dp_version_minor);
    RikerIO::Data::set(profile, rio_alloc, "rikerio.version.patch", dp_version_patch);

    memcpy(profile.ptr, &version_major, sizeof(uint16_t));
    memcpy((uint8_t*) profile.ptr + 2, &version_minor, sizeof(uint16_t));
    memcpy((uint8_t*) profile.ptr + 4, &version_patch, sizeof(uint16_t));

}
