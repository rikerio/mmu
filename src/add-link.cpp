#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "rikerio-automation/rikerio/rikerio.h"
#include "version.h"
#include "iostream"


int link_add(std::string profile_id, std::string link_key, std::vector<std::string> data_ids) {

    RikerIO::Profile profile;

    try {
        RikerIO::init(std::filesystem::path(RikerIO::root_path) / profile_id, profile);
    } catch (std::runtime_error& e) {
        std::cerr << "Error initializing Profile (" << profile_id << ")" << std::endl;
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    unsigned int err_count = 0;
    for (auto& id : data_ids) {
        if (RikerIO::Link::set(profile, link_key, id) == RikerIO::result_error) {
            err_count += 1;
        }
    }

    if (err_count > 0 ) {
        std::cerr << "Errors occured while adding links." << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;

}
