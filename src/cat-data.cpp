#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "rikerio-automation/rikerio/rikerio.h"
#include "version.h"
#include "iostream"
#include "iomanip"
#include "algorithm"


int data_cat(std::string profile_id, std::string data_id) {

    RikerIO::Profile profile;

    try {
        RikerIO::init(profile_id, profile);
    } catch (std::runtime_error& e) {
        std::cerr << "Error initializing Profile." << std::endl;
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    RikerIO::DataPoint data;

    if (RikerIO::Data::get(profile, data_id, data) == RikerIO::result_error) {
        std::cerr << "Error getting data." << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << std::to_string(data.byte_offset) << "." << std::to_string(data.bit_index);
    std::cout << " " << RikerIO::type_to_string(data.type);
    std::cout << " " << data.bit_size << std::endl;

    return EXIT_SUCCESS;
}
