#include "filesystem"
#include "rikerio/CLI11.h"
#include "mmu.h"
#include "iostream"
#include "rikerio-automation/rikerio/rikerio.h"
#include "version.h"
#include "rikerio/semver.h"
#include "sizestr.h"
#include "regex"
#include "sys/stat.h"
#include "sys/types.h"
#include "grp.h"
#include "unistd.h"

int mmu_create_main(std::string, std::string);
int mmu_remove_main(std::string);
int link_add(std::string, std::string, std::vector<std::string>);
int link_rm(std::string, std::string, std::vector<std::string>);
int link_cat(std::string, std::string, bool);
int data_cat(std::string, std::string);

int main(int argc, char** argv) {

    auto grp = getgrnam("rikerio");

    setgid(grp->gr_gid);

    umask(0007);


    SemanticVersion version(RIKERIO_MMU_VERSION);

    CLI::App app("RikerIO Command Line Interface.");

    std::filesystem::path root_path = "/var/lib/rikerio";
    std::string profile_id = "default";
    std::string size_str = "4K";
    std::string link_key = "";
    std::string data_id = "";
    std::string data_type = "";
    std::string filter = "";
    std::vector<std::string> data_ids;
    bool detail = false;
    bool purge = false;

    app.add_option("-p,--profile", profile_id, "Profile to be used.")->envname("RIO_PROFILE");

    // rio mmu create

    auto mmu_subc = app.add_subcommand("mmu")->fallthrough();
    auto mmu_create_subc = mmu_subc->add_subcommand("create")->callback([&]() {

        Size s(size_str);

        try {
            rikerio::MMU::create(root_path / profile_id, s);
        } catch (std::runtime_error& e) {
            std::cerr << e.what() << std::endl;
            rikerio::MMU::remove(root_path / profile_id);
            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    })->fallthrough();
    auto mmu_remove_subc = mmu_subc->add_subcommand("remove")->callback([&]() {
        rikerio::MMU::remove(root_path / profile_id, purge);
        return EXIT_SUCCESS;
    });

    mmu_create_subc->add_option("-i,--id", profile_id, "Profile ID");
    mmu_create_subc->add_option("-s,--size", size_str, "Shared Memory Size");

    mmu_remove_subc->add_flag("--purge", purge, "Remove Link Directory");

    // rio link

    auto link_subc = app.add_subcommand("link")->fallthrough();

    // rio link add

    auto link_add_subc = link_subc->add_subcommand("add")->callback([&profile_id, &link_key, &data_ids]() {
        link_add(profile_id, link_key, data_ids);
    })->fallthrough();
    link_add_subc->add_option("link", link_key, "Link Key to be used.")->required();
    link_add_subc->add_option("data", data_ids, "Data IDs to be added to the Link Key.")->required();

    // rio link rm

    auto link_rm_subc = link_subc->add_subcommand("rm")->callback([&profile_id, &link_key, &data_ids]() {
        link_rm(profile_id, link_key, data_ids);
    });
    link_rm_subc->add_option("link", link_key, "Link Key to be used.")->required();
    link_rm_subc->add_option("data", data_ids, "Data IDs to be added to the Link Key.");

    // rio link ls

    auto link_ls_subc = link_subc->add_subcommand("ls")->callback([&profile_id, &filter]() {
        std::string path = "/var/lib/rikerio/" + profile_id + "/links";
        for (const auto & entry : std::filesystem::directory_iterator(path))
            std::cout << entry.path().filename() << std::endl;
    });
    link_ls_subc->add_option("filter", filter, "Filter for Link Keys.");

    // rio link cat

    auto link_cat_subc = link_subc->add_subcommand("cat")->callback([&profile_id, &link_key, &detail]() {
        link_cat(profile_id, link_key, detail);
    })->fallthrough();
    link_cat_subc->add_option("link", link_key, "Link Key to be displayed.");
    link_cat_subc->add_flag("-d,--detail", detail, "List in Detail.");

    // rio link inspect

    auto link_inspect_subc = link_subc->add_subcommand("inspect")->callback([&profile_id, &link_key, &detail]() {
        link_cat(profile_id, link_key, true);
    })->fallthrough();
    link_inspect_subc->add_option("link", link_key, "Link Key to be displayed.");


    // rio data

    auto data_subc = app.add_subcommand("data")->fallthrough();

    // rio data ls

    auto data_ls_subc = data_subc->add_subcommand("ls")->callback([&profile_id, &filter]() {
        std::string path = "/var/lib/rikerio/" + profile_id + "/data";
        for (const auto & entry : std::filesystem::directory_iterator(path))
            std::cout << entry.path().filename() << std::endl;
    });
    data_ls_subc->add_option("filter", filter, "Filter for Data IDs.");

    // rio data cat

    auto data_cat_subc = data_subc->add_subcommand("cat")->callback([&profile_id, &data_id]() {
        data_cat(profile_id, data_id);
    });
    data_cat_subc->add_option("id", data_id, "Data ID.")->required();

    // rio version

    app.add_subcommand("version")->callback([&version]() {
        std::cout << version.get_version() << std::endl;
        return EXIT_SUCCESS;
    });

    CLI11_PARSE(app, argc, argv);

}
