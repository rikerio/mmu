#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "rikerio-automation/rikerio/rikerio.h"
#include "version.h"
#include "iostream"
#include "iomanip"
#include "algorithm"


int link_cat(std::string profile_id, std::string link_key, bool in_detail = false) {

    std::vector<RikerIO::DataPoint> data_points;

    RikerIO::Profile profile;

    try {
        RikerIO::init(profile_id, profile);
    } catch (std::runtime_error& e) {
        std::cerr << "Error initializing Profile." << std::endl;
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    std::vector<std::string> data_ids;

    if (RikerIO::Link::get(profile, link_key, data_ids) == RikerIO::result_error) {
        std::cerr << "Error getting link." << std::endl;
        return EXIT_FAILURE;
    }

    unsigned int max_length = 0;

    for (auto &id : data_ids) {
        max_length = std::max(static_cast<unsigned int>(id.length()), max_length);
    }

    unsigned int err_count = 0;
    for (auto& id : data_ids) {

        RikerIO::DataPoint dp;

        std::cout << std::left << std::setw(max_length + 1) << id;

        if (!in_detail) {
            std::cout << std::endl;
            continue;
        }

        if (RikerIO::Data::get(profile, id, dp) == RikerIO::result_error) {
            err_count += 1;
            std::cout << std::endl;
            continue;
        }

        std::string tmp = std::to_string(dp.byte_offset) + "." + std::to_string(dp.bit_index);

        std::cout << std::setw(7) << std::right << tmp << " ";
        std::cout << std::setw(8) << std::left << RikerIO::type_to_string(dp.type);
        std::cout << std::setw(7) << std::left << dp.bit_size << std::endl;
    }

    return err_count == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
