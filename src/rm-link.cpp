#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "rikerio-automation/rikerio/rikerio.h"
#include "version.h"
#include "iostream"

int link_rm(std::string profile_id, std::string link_key, std::vector<std::string> data_ids) {

    RikerIO::Profile profile;

    try {
        RikerIO::init(profile_id, profile);
    } catch (std::runtime_error& e) {
        std::cerr << "Error initializing Profile." << std::endl;
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    if (data_ids.size() == 0) {

        if (RikerIO::Link::remove(profile, link_key) == RikerIO::result_error) {
            std::cerr << "Error removing link." << std::endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;

    }

    unsigned int err_count = 0;
    for (auto& id : data_ids) {

        if (RikerIO::Link::remove(profile, link_key, id) == RikerIO::result_error) {
            err_count += 1;
        };

    }

    if (err_count > 0) {
        std::cerr << "Errors during operation." << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;

}
