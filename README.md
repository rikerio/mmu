# RikerIO Memory Management Unit

RikerIO MMU is part of the RikerIO Toolset. The MMU helps managing a shared memory area and associating PDO data with keys and links
.
![RikerIO Sketch](doc/sketch.png)

## Build

The Library is build using Meson, simply do the following from the root directory:
```
meson ./build
cd build
ninja
```

## Create a configuration

For example in /etc/rikerio/default.yaml

```
size: 4096
cycle: 10000
```

## Start the Server

Best way is to start this as the root user.

```
rio mmu start --config=/etc/rikerio/default.yaml
```

## Inspect Data and Links

You can inspect data and links with the following commands:

```
rio link ls
```

```
rio data ls
```

## License
LGPLv3  
